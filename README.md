# training_project

This repository is for the project training, which contain reading/writing json file for different personal and bank accounts information.

## Getting started

clone this project into your system, using the following command;
```
git clone https://gitlab.com/Munib-Ahmed/training_project.git
``` 
Enter the cloned Directory, with:
```
cd training_project
```
Now, We need to create a directory typically named as "build";
```
mkdir build
```

navigate to build directory;
```
cd build
```
Enter the following commands to make an executable;

```
cmake ..
cmake --build .
./training_project
```