#ifndef PERSONAL_DATA_H
#define PERSONAL_DATA_H
#include "personal_data_struct.h"
namespace emumba
{
    namespace training
    {
        class personal_data
        {
        private:
            personal_data_struct personal_info;

        public:
            void set_age(const int& age);
            void set_city(const std::string& city);
            void set_coordinates(const float& lon, const float& lat);

            const int& get_age() const;
            const std::string& get_city() const;
            const float& get_coordinates_lat() const ;
            const float& get_coordinates_long() const;
        };
    }
}

#endif
