#ifndef ACCOUNT_DATA_STRUCT_H
#define ACCOUNT_DATA_STRUCT_H
#include <string>

namespace emumba
{
  namespace training
  {
    struct account_data_struct
    {
      // add variables for person's name, bank, float-type account balance and currency
      std::string name;
      std::string bank;
      float account_balance;
      std::string currency;
    };
  }
}
#endif
