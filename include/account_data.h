#ifndef ACCOUNT_DATA_H
#define ACCOUNT_DATA_H
#include "account_data_struct.h"

namespace emumba
{
    namespace training
    {
        class account_data
        {
        private:
            account_data_struct account_info;

        public:
            void set_name(const std::string& name);
            void set_bank_name(const std::string& bank_name);
            void set_account_balance(const float& account_balance);
            void set_currency(const std::string& currency);
            
            const std::string& get_name() const;
            const std::string& get_bank_name() const;
            const float& get_account_balance() const;
            const std::string& get_currency() const;
        };
    }
}
#endif
