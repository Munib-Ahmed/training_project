#ifndef PERSONAL_DATA_STRUCT_H
#define PERSONAL_DATA_STRUCT_H
#include <string>
#include <vector>
namespace emumba
{
  namespace training
  {
    struct personal_data_struct
    {
      //add variables for age, city and a float-type vector of coordinates
      int age;
      std::string city;
      std::vector<float> coordinates;
    };
  }
}
#endif
