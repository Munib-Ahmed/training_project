#ifndef WHITE_LIST_H
#define WHITE_LIST_H

#include "account_data.h"
#include "personal_data.h"
#include <vector>
#include "flat_hash_map.hpp"
#include "nlohmann/json.hpp"
using json = nlohmann::json;

namespace emumba
{
    namespace training
    {
        class white_list
        {
        private:
            ska::flat_hash_map<std::string, personal_data *> personal_data_map;
            ska::flat_hash_map<std::string, account_data *> account_data_map;
            std::vector<personal_data> personal_data_base;
            std::vector<account_data> account_data_base;

            json find_query_type(const json &) const;
            json find_wrt_personal_data(const json &) const;
            json find_wrt_account_data(const json &) const;
            void add_to_vector_personal_data(const personal_data &);
            void add_to_vector_account_data(const account_data &);
            void add_to_whitelist_personal_data(const std::vector<std::string> &);
            void add_to_whitelist_account_data(const std::vector<std::string> &);

        public:
            bool is_present_personal(const std::string &) const;
            bool is_present_account(const std::string &) const;
            void print_personal_data(const std::string &) const;
            void print_account_data(const std::string &) const;
            void print_all_personal() const;
            void print_all_account() const;

            bool initialize_data_base();
            void poll_queries() const;
            bool file_verification();
        };
    }
}
#endif