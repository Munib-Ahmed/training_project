#include "white_list.h"
using namespace emumba::training;

int main()
{
    white_list w;

    if (w.file_verification())
    {
        if (w.initialize_data_base())
        {
            w.poll_queries();
        }
    }
    return 0;
}