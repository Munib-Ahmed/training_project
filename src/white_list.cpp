
#include "spdlog/spdlog.h"
#include <fstream>
#include "white_list.h"
#include <iostream>
#include <chrono>
#include <thread>

using namespace std::this_thread; // sleep_for, sleep_until
using namespace std::chrono;      // nanoseconds, system_clock, seconds
using namespace std;
using namespace emumba::training;

void white_list::add_to_vector_personal_data(const personal_data &person_info)
{
    personal_data_base.push_back(person_info);
    // personal_data_map[person_name] = &personal_data_base.back();
}

void white_list::add_to_vector_account_data(const account_data &account_info)
{
    account_data_base.push_back(account_info);
    // account_data_map[account_id] = &account_data_base.back();
}

void white_list::add_to_whitelist_personal_data(const std::vector<std::string> &name_list)
{
    int i = 0;
    for (auto itr_name = name_list.begin(); itr_name != name_list.end(); ++itr_name)
    {
        personal_data_map[*itr_name] = &personal_data_base[i];
        i = i + 1;
    }
}
void white_list::add_to_whitelist_account_data(const std::vector<std::string> &account_data_list)
{
    int i = 0;
    for (auto itr_account = account_data_list.begin(); itr_account != account_data_list.end(); ++itr_account)
    {
        account_data_map[*itr_account] = &account_data_base[i];
        i = i + 1;
    }
}

bool white_list::is_present_personal(const std::string &person_name) const
{
    if (personal_data_map.find(person_name) != personal_data_map.end())
    {
        spdlog::info("Person is present");
        return true;
    }
    spdlog::info("Person is not present");
    return false;
}

bool white_list::is_present_account(const std::string &account_id) const
{
    if (account_data_map.find(account_id) != account_data_map.end())
    {
        spdlog::info("Person account is present in list");
        return true;
    }
    spdlog::info("Person account is not present in list");
    return false;
}

void white_list::print_personal_data(const std::string &person_name) const
{
    auto itr = personal_data_map.find(person_name);
    cout << "person name = " << person_name << endl;
    cout << "person age = " << itr->second->get_age();
    cout << "person city = " << itr->second->get_city();
    cout << "person coordinates long = " << itr->second->get_coordinates_long();
    cout << "person coordinates lat = " << itr->second->get_coordinates_lat();
}

void white_list::print_account_data(const std::string &account_id) const
{
    auto itr = account_data_map.find(account_id);
    cout << "account id = " << account_id << endl;
    cout << "person currency = " << itr->second->get_currency();
    cout << "person balance = " << itr->second->get_account_balance();
    cout << "person name = " << itr->second->get_name();
    cout << "person bank name = " << itr->second->get_bank_name();
}

void white_list::print_all_personal() const
{
    cout << endl
         << "printing personal result" << endl;
    for (auto itr = personal_data_base.begin(); itr != personal_data_base.end(); itr++)
    {
        cout << "age = " << itr->get_age() << endl;
        cout << "city = " << itr->get_city() << endl;
        cout << "coordi log = " << itr->get_coordinates_long() << endl;
        cout << "coordi lat = " << itr->get_coordinates_lat() << endl;
        cout << endl
             << endl;
    }
}

void white_list::print_all_account() const
{
    cout << endl
         << "printing account result" << endl;
    for (auto itr = account_data_base.begin(); itr != account_data_base.end(); itr++)
    {
        cout << "name = " << itr->get_name() << endl;
        cout << "bank name = " << itr->get_bank_name() << endl;
        cout << "account balance = " << itr->get_account_balance() << endl;
        cout << "currency = " << itr->get_currency() << endl;
        cout << endl
             << endl;
    }
}

bool white_list::file_verification()
{
    spdlog::set_level(spdlog::level::trace);
    spdlog::trace("The program has started.");

    return true;
}

bool white_list::initialize_data_base()
{
    bool file_read = true;
    json people;
    vector<std::string> name_list;
    vector<std::string> account_data_list;

    try
    {
        std::ifstream people_file("json_files/personal_data.json");
        json json_object = json::parse(people_file);
    }
    catch (...)
    {
        spdlog::error("File not read, maybe some error in json file format/paranthesis");
        file_read = false;
    }

    if (file_read == true)
    {
        std::ifstream people_file("json_files/personal_data.json");
        if (people_file.fail())
        {
            spdlog::error("personal_data.json file is not read or is empty");
            file_read = false;
        }
        else
        {

            people_file >> people;
            if (people["Account Holders Info"].is_array())
            {
                for (int i = 0; people["Account Holders Info"][i] != nullptr; i++)
                {
                    personal_data person_object;
                    if (people["Account Holders Info"][i]["age"].is_number() && people["Account Holders Info"][i]["city"].is_string() && people["Account Holders Info"][i]["coordinates"]["long"].is_number() && people["Account Holders Info"][i]["coordinates"]["lat"].is_number() && people["Account Holders Info"][i]["name"].is_string())
                    {
                        person_object.set_age((people["Account Holders Info"][i]["age"]));
                        person_object.set_city((people["Account Holders Info"][i]["city"]));
                        person_object.set_coordinates((people["Account Holders Info"][i]["coordinates"]["long"]), (people["Account Holders Info"][i]["coordinates"]["lat"]));
                        add_to_vector_personal_data(person_object);
                        name_list.push_back(people["Account Holders Info"][i]["name"]);
                    }

                    if (!people["Account Holders Info"][i]["accounts"].empty())
                    {
                        for (long unsigned int j = 0; j < people["Account Holders Info"][i]["accounts"].size(); j++)
                        {
                            account_data account_object;
                            if (people["Account Holders Info"][i]["name"].is_string() && people["Account Holders Info"][i]["accounts"][j]["currency"].is_string() && people["Account Holders Info"][i]["accounts"][j]["bank"].is_string() && people["Account Holders Info"][i]["accounts"][j]["account id"].is_string() && people["Account Holders Info"][i]["accounts"][j]["balance"].is_number())
                            {
                                account_object.set_name(people["Account Holders Info"][i]["name"]);
                                account_object.set_currency((people["Account Holders Info"][i]["accounts"][j]["currency"]));
                                account_object.set_bank_name((people["Account Holders Info"][i]["accounts"][j]["bank"]));
                                account_object.set_account_balance((people["Account Holders Info"][i]["accounts"][j]["balance"]));
                                add_to_vector_account_data(account_object);
                                account_data_list.push_back(people["Account Holders Info"][i]["accounts"][j]["account id"]);
                            }
                        }
                    }
                }
                file_read = true;
                spdlog::info("All the data has been added to our list.");
                add_to_whitelist_personal_data(name_list);
                add_to_whitelist_account_data(account_data_list);
            }
            else
            {
                spdlog::info("Account holder info not found in personal data file");
                file_read = false;
            }

            spdlog::trace("printing entered data");
            // if printing is required
            // print_all_personal();
            // print_all_account();

            people.dump();
            people_file.close();
        }
    }
    return file_read;
}

void white_list::poll_queries() const
{

    string present_query_file = "";
    string previous_query_file = "";
    json j;

    while (1)
    {
        json queries;
        spdlog::info("Reading the Query file");

        try
        {
            std::ifstream query_file("json_files/query.json");
            json json_object = json::parse(query_file);
        }
        catch (...)
        {
            spdlog::error("File not read, maybe some issue in json file paranthesis");
            return;
        }
        std::ifstream query_file("json_files/query.json");
        if (query_file.fail())
        {
            spdlog::error("File not read");
        }
        else
        {
            query_file >> queries;
            present_query_file = queries.dump();

            if (present_query_file == previous_query_file)
            {
                spdlog::info("Query file didn't changed!");
            }
            else
            {

                spdlog::info("Query file initialized/changed !!! \t Working on it ....");

                string str_clear = "";
                // run till number of queries in quert file
                for (int i = 0; queries["query"][i] != nullptr; i++)
                {
                    if (queries["query"][i].begin().value().is_string() && str_clear != "clear")
                    {
                        str_clear = queries["query"][i].begin().value();
                    }
                    // if(queries["query"][i]["search"].)
                    j[i] = find_query_type(queries["query"][i]);
                }
                if (str_clear != "clear")
                {
                    std::ofstream outfile("json_files/result_query.json"); // std::ofstream outfile("../json_files/result_query.json", ios::app);
                    outfile << std::setw(4) << j << endl
                            << endl;
                    outfile.close();
                }
                else
                {
                    spdlog::info("Clearing the result file");
                    std::ofstream outfile("json_files/result_query.json"); // std::ofstream outfile("../json_files/result_query.json", ios::app);
                    outfile.close();
                }
                spdlog::info("Queries are entertained.");
            }

            previous_query_file = present_query_file;
            sleep_for(seconds(5));
            sleep_until(system_clock::now() + seconds(1));
        }
    }
}

json white_list::find_query_type(const json &single_query) const
{
    auto it = single_query.begin();
    if (((it.key() == "name") || (it.key() == "age") || (it.key() == "city") || (it.key() == "long") || (it.key() == "lat")) && (!it.value().is_array()))
    {
        return find_wrt_personal_data(single_query);
    }
    else if (it.key() == "account_id" || it.key() == "bank" || it.key() == "balance" || it.key() == "currency")
    {
        return find_wrt_account_data(single_query);
    }
    else
    {

        return NULL;
    }
}

json white_list::find_wrt_personal_data(const json &single_query) const
{
    auto it = single_query.begin();
    vector<string> names_list;

    for (auto itr_personal = personal_data_map.begin(); itr_personal != personal_data_map.end(); itr_personal++)
    {
        if ((it.key() == "name" && it.value() == itr_personal->first) || (it.key() == "age" && it.value() == itr_personal->second->get_age()) || (it.key() == "city" && it.value() == itr_personal->second->get_city()) || (it.key() == "lat" && it.value() == itr_personal->second->get_coordinates_lat()) || (it.key() == "long" && it.value() == itr_personal->second->get_coordinates_long()))
        {
            if (!(std::find(names_list.begin(), names_list.end(), itr_personal->first) != names_list.end()) || names_list.empty())
            {
                names_list.push_back(itr_personal->first);
            }
        }
    }
    int inc_personal = 0;
    int inc_account = 0;
    json j;
    if (single_query["search"].size() > 0)
    {
        for (vector<string>::iterator itr_list = names_list.begin(); itr_list != names_list.end(); itr_list++)
        {
            for (auto itr_personal = personal_data_map.begin(); itr_personal != personal_data_map.end(); itr_personal++)
            {
                bool increment = false;
                if (itr_personal->first == *itr_list)
                {
                    for (long unsigned int i = 0; i < single_query["search"].size(); i++)
                    {
                        if (single_query["search"][i] == "user" || single_query["search"][i] == "name")
                        {
                            j[it.key()]["personal"][inc_personal]["Name"] = itr_personal->first;
                            increment = true;
                        }
                        else if (single_query["search"][i] == "age")
                        {
                            j[it.key()]["personal"][inc_personal]["Age"] = itr_personal->second->get_age();
                            increment = true;
                        }
                        else if (single_query["search"][i] == "city")
                        {
                            j[it.key()]["personal"][inc_personal]["City"] = itr_personal->second->get_city();
                            increment = true;
                        }
                        else if (single_query["search"][i] == "long")
                        {
                            j[it.key()]["personal"][inc_personal]["long"] = itr_personal->second->get_coordinates_long();
                            increment = true;
                        }
                        else if (single_query["search"][i] == "lat")
                        {
                            j[it.key()]["personal"][inc_personal]["lat"] = itr_personal->second->get_coordinates_lat();
                            increment = true;
                        }
                    }
                }
                if (increment == true)
                {
                    inc_personal = inc_personal + 1;
                }
            }
            for (auto itr_account = account_data_map.begin(); itr_account != account_data_map.end(); itr_account++)
            {
                bool increment = false;

                if (itr_account->second->get_name() == *itr_list)
                {
                    for (long unsigned int i = 0; i < single_query["search"].size(); i++)
                    {
                        if (single_query["search"][i] == "account_id")
                        {
                            j[it.key()]["accounts"][inc_account]["Account_id"] = itr_account->first;
                            increment = true;
                        }
                        else if (single_query["search"][i] == "bank")
                        {
                            j[it.key()]["accounts"][inc_account]["Bank"] = itr_account->second->get_bank_name();
                            increment = true;
                        }
                        else if (single_query["search"][i] == "balance")
                        {
                            j[it.key()]["accounts"][inc_account]["Balance"] = itr_account->second->get_account_balance();
                            increment = true;
                        }
                        else if (single_query["search"][i] == "currency")
                        {
                            j[it.key()]["accounts"][inc_account]["Currency"] = itr_account->second->get_currency();
                            increment = true;
                        }
                    }
                }
                if (increment == true)
                {
                    inc_account = inc_account + 1;
                }
            }
        }
        return j;
    }
    else
    {
        spdlog::info("The provided search parameter is empty");
    }
    return NULL;
}

json white_list::find_wrt_account_data(const json &single_query) const
{
    auto it = single_query.begin();
    vector<string> names_list;

    for (auto itr_account = account_data_map.begin(); itr_account != account_data_map.end(); itr_account++)
    {
        if ((it.key() == "account_id" && it.value() == itr_account->first) || (it.key() == "bank" && it.value() == itr_account->second->get_bank_name()) || (it.key() == "balance" && it.value() == itr_account->second->get_account_balance()) || (it.key() == "currency" && it.value() == itr_account->second->get_currency()))
        {
            if (!(std::find(names_list.begin(), names_list.end(), itr_account->second->get_name()) != names_list.end()) || names_list.empty())
            {
                names_list.push_back(itr_account->second->get_name());
            }
        }
    }

    int inc_personal = 0;
    int inc_account = 0;
    json j;
    if (single_query["search"].size() > 0)
    {
        for (vector<string>::iterator itr_list = names_list.begin(); itr_list != names_list.end(); itr_list++)
        {
            for (auto itr_personal = personal_data_map.begin(); itr_personal != personal_data_map.end(); itr_personal++)
            {
                bool increment = false;
                if (itr_personal->first == *itr_list)
                {
                    for (long unsigned int i = 0; i < single_query["search"].size(); i++)
                    {
                        if (single_query["search"][i] == "user" || single_query["search"][i] == "name")
                        {
                            j[it.key()]["personal"][inc_personal]["Name"] = itr_personal->first;
                            increment = true;
                        }
                        else if (single_query["search"][i] == "age")
                        {
                            j[it.key()]["personal"][inc_personal]["Age"] = itr_personal->second->get_age();
                            increment = true;
                        }
                        else if (single_query["search"][i] == "city")
                        {
                            j[it.key()]["personal"][inc_personal]["City"] = itr_personal->second->get_city();
                            increment = true;
                        }
                        else if (single_query["search"][i] == "long")
                        {
                            j[it.key()]["personal"][inc_personal]["long"] = itr_personal->second->get_coordinates_long();
                            increment = true;
                        }
                        else if (single_query["search"][i] == "lat")
                        {
                            j[it.key()]["personal"][inc_personal]["lat"] = itr_personal->second->get_coordinates_lat();
                            increment = true;
                        }
                    }
                }
                if (increment == true)
                {
                    inc_personal = inc_personal + 1;
                }
            }
            for (auto itr_account = account_data_map.begin(); itr_account != account_data_map.end(); itr_account++)
            {
                bool increment = false;

                if ((itr_account->second->get_name() == *itr_list))
                {
                    bool decision = false;
                    string str_search = "";
                    if (it.key() == "account_id" || it.key() == "bank" || it.key() == "currency")
                    {
                        str_search = it.value();
                    }

                    if ((it.key() == "account_id" && str_search == itr_account->first) || (it.key() == "bank" && str_search == itr_account->second->get_bank_name()) || (it.key() == "balance" && it.value() == itr_account->second->get_account_balance()) || (it.key() == "currency" && str_search == itr_account->second->get_currency()))
                    {
                        decision = true;
                    }

                    if (decision == true)
                    {
                        for (long unsigned int i = 0; i < single_query["search"].size(); i++)
                        {
                            if (single_query["search"][i] == "account_id")
                            {
                                j[it.key()]["accounts"][inc_account]["Account_id"] = itr_account->first;
                                increment = true;
                            }
                            else if (single_query["search"][i] == "bank")
                            {
                                j[it.key()]["accounts"][inc_account]["Bank"] = itr_account->second->get_bank_name();
                                increment = true;
                            }
                            else if (single_query["search"][i] == "balance")
                            {
                                j[it.key()]["accounts"][inc_account]["Balance"] = itr_account->second->get_account_balance();
                                increment = true;
                            }
                            else if (single_query["search"][i] == "currency")
                            {
                                j[it.key()]["accounts"][inc_account]["Currency"] = itr_account->second->get_currency();
                                increment = true;
                            }
                        }
                    }
                }
                if (increment == true)
                {
                    inc_account = inc_account + 1;
                }
            }
        }
        return j;
    }
    else
    {
        spdlog::info("Search is empty");
    }
    return NULL;
}