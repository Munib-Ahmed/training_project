#include "personal_data.h"
using namespace emumba::training;

void personal_data::set_age(const int& age)
{
    personal_info.age = age;
}

void personal_data::set_city(const std::string& city)
{
    personal_info.city = city;
}

const int& personal_data::get_age() const
{
    return personal_info.age;
}

const std::string& personal_data::get_city() const
{
    return personal_info.city;
}

void personal_data::set_coordinates(const float& lon, const float& lat)
{
    personal_info.coordinates.push_back(lon);
    personal_info.coordinates.push_back(lat);
}

const float& personal_data::get_coordinates_lat() const
{
    return personal_info.coordinates.back();
}

const float& personal_data::get_coordinates_long() const
{
    return personal_info.coordinates.front();
}