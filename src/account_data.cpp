#include "account_data.h"
using namespace emumba::training;


void account_data::set_name(const std::string& name)
{
    account_info.name=name;
}

void account_data::set_bank_name(const std::string& bank_name)
{
    account_info.bank=bank_name;
}

void account_data::set_account_balance(const float& account_balance)
{
    account_info.account_balance=account_balance;
}

void account_data::set_currency(const std::string& currency)
{
    account_info.currency=currency;
}

const std::string& account_data::get_name() const
{
    return account_info.name;
}

const std::string& account_data::get_bank_name() const
{
    return account_info.bank;
}

const float& account_data::get_account_balance() const
{
    return account_info.account_balance;
}

const std::string& account_data::get_currency() const
{
    return account_info.currency;
}